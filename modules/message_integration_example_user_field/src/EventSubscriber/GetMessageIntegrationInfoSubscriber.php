<?php

namespace Drupal\message_integration_example_user_field\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\message_integration\Event\GetMessageIntegrationInfo;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GetMessageIntegrationInfoSubscriber.
 *
 * @package Drupal\message_integration_example_user_field\EventSubscriber
 */
class GetMessageIntegrationInfoSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a UserApprovalForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      // Static class constant => method on this class.
      GetMessageIntegrationInfo::EVENT_NAME => 'onNodeSave',
    ];
  }

  /**
   * Subscribe to the user login event dispatched.
   *
   * @param \Drupal\message_integration\Event\GetMessageIntegrationInfo $event
   *   Dat event object yo.
   */
  public function onNodeSave(GetMessageIntegrationInfo $event) {
    $node = $event->node;
    $current_entity_lang = $node->get('langcode')->value;
    $translated_entity = $node->getTranslation($current_entity_lang);
    $mod = $translated_entity->get('moderation_state')->getString();

    $template = match ($mod) {
      'published' => 'publish_node',
      'ready_to_review' => 'ready_to_review_node',
      'change_requested' => 'change_requested_node',
      'ready_for_publication' => 'ready_for_publication_node',
      default => 'update_node',
    };
    $node->template = $template;

    // Get Email list
    $uids = $node->get("field_users_to_email");
    $emails = [];
    if (isset($uids)) {
      foreach ($uids as $uid) {
        $user = $this->entityTypeManager->getStorage('user')->load($uid->target_id);
        if ($user) {
          array_push($emails, $user->id());
        }
      }
    }
    $node->email_list = $emails;
  }

}
