In this scenario, we added a user reference field to the content types we wanted to use send emails from.
In our subscriber we pulled that field to get the User IDs list to email.
Then using our specific moderation states mapped them to custom message templates.
