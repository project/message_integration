# Message Integration Implementation

The goal of this module is to create a base module that utilizes a message stack that can be built on.
TODO Better description

 The following modules are used:

- [Diff](https://www.drupal.org/project/diff)
- [Message](https://www.drupal.org/project/message)
- [Message Notify](https://www.drupal.org/project/message_notify)
- [Message Subscribe](https://www.drupal.org/project/message_subscribe)
- [Message UI](https://www.drupal.org/project/message_ui)
- [Mailsystem](https://www.drupal.org/project/mailsystem)
- [Token](https://www.drupal.org/project/token)
- [tijsverkoyen/css-to-inline-styles](https://github.com/tijsverkoyen/CssToInlineStyles)

## Using this code
To best use this module users should create custom modules that fit their needs for getting an email list with templates
that fit their project.

The custom module should subscribe to the event from the Message Integration
Example from the message_integration_example_user_field submodule
```
services:
  message_integration_message_integration_info:
    class: '\Drupal\message_integration_example_user_field\EventSubscriber\GetMessageIntegrationInfoSubscriber'
    arguments: ['@entity_type.manager']
    tags:
      - { name: 'event_subscriber' }
```

Then inside the subscriber do some logic to get the list of user ID's and assign it to the $node->email_list variable.
Next figure out which template you need to use.  Once you do assign it to $node->template.

That's it!

The main module will then use that info to create the list.

## Recommendations
Any new templates should use a field_node_reference and field_published field.
If you want to use the diff module also use a field_new_vid and field_original_vid field.
