<?php

namespace Drupal\message_integration;

use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Asset\AttachedAssets;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

/**
 * Provides a class that implements TrustedCallbackInterface.
 */
class MessageIntegrationPostRender implements TrustedCallbackInterface {

  /**
   * Constructs the MessageIntegrationPostRender class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   A cache backend interface.
   * @param \Drupal\Core\Asset\AssetResolverInterface $assetResolver
   *   The asset resolver.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(
    protected CacheBackendInterface $cache,
    protected AssetResolverInterface $assetResolver,
    protected FileSystemInterface $fileSystem
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['messageIntegrationPostRender'];
  }

  /**
   * Post-render callback function.
   *
   * @param string $markup
   *   The rendered element.
   * @param array $element
   *   The element which was rendered (for reference)
   *
   * @return string
   *   Markup altered as necessary.
   */
  public function messageIntegrationPostRender(string $markup, array $element): string {
    $cid = 'message_integration_css';
    $css = NULL;
    $text = '';

    if ($cache = $this->cache->get($cid)) {
      $css = $cache->data;
    }
    else {
      $assets = AttachedAssets::createFromRenderArray([
        '#attached' => [
          'library' => [
            'core/normalize',
            'olivero/global-styling',
            'diff/diff.single_column',
            'diff/diff.colors',
            'diff/diff.general',
          ],
        ],
      ]);

      foreach ($this->assetResolver->getCssAssets($assets, FALSE) as $css_asset) {
        $css .= file_get_contents($this->fileSystem->realpath($css_asset['data']));
      }
      $this->cache->set($cid, $css);
    }
    // For email, we don't want linked css files in the HEAD of the page, we
    // want css to be inlined into the body. So we construct a single string of
    // css, then use CssToInlineStyles() to render that css inline into markup.
    if ($css) {
      $text = (new CssToInlineStyles())->convert($markup, $css);
    }
    return $text;
  }

}
