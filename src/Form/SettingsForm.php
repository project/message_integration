<?php

namespace Drupal\message_integration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for Settings Form of Message Integration module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Constructs a new MessageIntegrationForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration Interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   */
  public function __construct(ConfigFactoryInterface $config, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->configManager = $config;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'message_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('message_integration.settings');
    $form['skip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip'),
      '#description' => $this->t('Skip sending messages when content is updated  if this setting is checked.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('skip'),
    ];

    $allowed_content_entity_types = $config->get('allowed_content_entities');
    $form['allowed_content_entities'] = [
      '#type' => 'details',
      '#open' => !empty($allowed_content_entity_types),
      '#title' => $this->t('Content Bundles to email'),
      '#description' => $this->t('In case no entity type is selected then message integration is enabled on all content bundles forms.'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="active_on[content_entity_forms]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $bundles_info = $this->entityTypeBundleInfo->getBundleInfo('node');
    $allowed_bundles = !empty($allowed_content_entity_types['node']['bundles']) ? $allowed_content_entity_types['node']['bundles'] : [];
    $bundles = [];
    foreach ($bundles_info as $key => $bundle) {
      $bundles[$key] = $bundle['label'];
    }

    $form['allowed_content_entities']['node']['bundles'] = [
      '#type' => 'checkboxes',
      '#default_value' => $allowed_bundles,
      '#options' => $bundles,
      '#prefix' => '<div class="panel">',
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $allowed_content_entity_types = [];
    foreach ($form_state->getValue('allowed_content_entities') as $entity_type_id => $data) {
      $allowed_bundles = array_filter($data['bundles']);
      $allowed_content_entity_types[$entity_type_id]['bundles'] = $allowed_bundles;
    }
    $this->config('message_integration.settings')
      ->set('skip', $form_state->getValue('skip'))
      ->set('allowed_content_entities', $allowed_content_entity_types)
      ->save();
  }

}
