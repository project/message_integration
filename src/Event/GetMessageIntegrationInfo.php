<?php

namespace Drupal\message_integration\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\node\Entity\Node;

/**
 * Event that is fired when a user logs in.
 */
class GetMessageIntegrationInfo extends Event {

  const EVENT_NAME = 'message_integration_node_presave';

  /**
   * The user account.
   *
   * @var \Drupal\node\Entity\Node
   */
  public Node $node;

  /**
   * Constructs the object.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   */
  public function __construct(Node $node) {
    $this->node = $node;
  }

}
